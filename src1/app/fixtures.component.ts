import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Request, XHRBackend, XHRConnection } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { FixturesService } from './fixtures.service';
import { HttpClient } from '@angular/common/http';
import { InterceptorModule } from './fixtures.service';

//require('highcharts');
declare var require: any


@Component({
    selector: 'fixtures-section',
    templateUrl: './fixtures.component.html',
    styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent {
    require: any;

    public data: any[];
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";
    public leagues = [];
    public numberOfTeams = [];
    public numberOfGames = [];
    constructor(private http: Http, private fixturesService: FixturesService, private httpclient: HttpClient) {

    }
    ngOnInit(): void {
        var Highcharts = require('highcharts');

        // Load module after Highcharts is loaded
        require('highcharts/modules/exporting')(Highcharts);

        const headers = new Headers();
        headers.append('X-Auth-Token', '3082de7283c74f83aea4c9348bf511ad');
        let options = new RequestOptions({ headers: headers });
        this.http.get("http://api.football-data.org//v1/competitions/", options)
            .map((res: Response) => {
                this.data = res.json();
                console.log(this.data);
                for (var i = 0; i < this.data.length; ++i) {
                    this.leagues.push(this.data[i].caption);
                    this.numberOfGames.push(this.data[i].numberOfGames);
                    this.numberOfTeams.push(this.data[i].numberOfTeams);
                }

                Highcharts.chart('chart2', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Number of Games and Teams playing in each league in 2017/2018'
                    },
                    xAxis: {
                        categories: this.leagues,
                        title: {
                            text: 'Leagues'
                        }
                    },

                    yAxis: {
                        title: {
                            text: 'Number of Games and Teams'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },

                    series: [{
                        name: 'Number Of Games',
                        data: this.numberOfGames
                    }, {
                        name: 'Number Of Teams',
                        data: this.numberOfTeams
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });

            })
            .subscribe((data: any) => {
            });

    }



    //  options: Object;


}