import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable, NgModule } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpHeaders, HTTP_INTERCEPTORS, HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class FixturesService implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const dupReq = req.clone({headers: req.headers.set('Access-Control-Allow-Origin','http://api.football-data.org/')});
        return next.handle(dupReq);
    }
    /*BASE_URL = 'http://api.football-data.org/';

    constructor(private http: Http){}

    getFixtures(){
       let headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
        let options = new RequestOptions({ headers: headers });
        return this.http.get('http://api.football-data.org/v1/fixtures/', { headers: this.getHeaders()})
        .map((res: Response) => res.json());
        /*.subscribe((res: Response) => {
         var payload = res.json();
         var headers = res.headers;
         
        });*/
       /*}

       private getHeaders(){*/
    // I included these headers because otherwise FireFox
    // will request text/html instead of application/json
    /*let headers = new Headers();
    headers.append('Accept', 'application/json, charset=UTF-8');
    headers.append('Access-Control-Allow-Headers','http://api.football-data.org/');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    headers.append('X-Response-Control', 'minified');
    return headers;
  }*/
    };

    @NgModule({
        providers:[{provide: HTTP_INTERCEPTORS, useClass: FixturesService, multi: true}]
    })

    export class InterceptorModule{}
