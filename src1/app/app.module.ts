import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CarouselModule } from 'angular4-carousel';
import { HttpModule, RequestOptions } from '@angular/http';
import { ChartModule } from 'angular2-highcharts';
import { Http } from '@angular/http';
import {DataTableModule} from "angular2-datatable";
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { TeamComponent } from './teams.component';
import { FixturesComponent } from './fixtures.component';
import { SeasonsComponent } from './seasons.component';

import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { FixturesService } from './fixtures.service';
import { InterceptorModule } from './fixtures.service';
//declare var require: any

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TeamComponent,
    FixturesComponent,
    SeasonsComponent
  ],
  imports: [
    BrowserModule,
    CarouselModule,
    HttpModule,
    DataTableModule,
    ChartModule,
    HttpClientModule,
    InterceptorModule,
    ScrollToModule.forRoot(),
    HttpClientJsonpModule
  ],
  providers: [FixturesService],
  bootstrap: [AppComponent, HeaderComponent, TeamComponent]
})
export class AppModule { }
