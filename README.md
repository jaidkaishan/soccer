# Soccer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.6.

##Install Node Package Manager

Run `npm install` for installing node packages that will be used by App.

##If Running `npm install` shows any error of Vulnerabilities

Run `npm rebuild node-sass --force`, this will solve the issue of Vulnerabilities.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Use (http://api.football-data.org/) to generate the API key and use it for data.

Change the existing API key in X-AUTH-TOKEN attribute in teams.component.ts and fixtures.component.ts files.
