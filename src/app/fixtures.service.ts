import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable, NgModule } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpHeaders, HTTP_INTERCEPTORS, HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import {Fixture} from './Fixture';
import {Games} from './games'

const headers = new Headers();
        headers.append('X-Auth-Token', 'c5c4600e9a974ffcad5f4773fba6bd1f');
        let options = new RequestOptions({ headers: headers });

@Injectable()



export class FixturesService implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const dupReq = req.clone({headers: req.headers.set('Access-Control-Allow-Origin','http://api.football-data.org/')});
        return next.handle(dupReq);
    }
    BASE_URL = 'http://api.football-data.org/';

    constructor(private http: Http){}

    
    getLastGame(){
        return this.http.get('http://api.football-data.org/v1/teams/66/fixtures?timeFrame=p20', options)
        .map((response: Response) => {
                    return response.json();
            })
    }

    getResultsTable(){     
        return this.http.get('http://api.football-data.org/v1/teams/66/fixtures', options)
        .map((response: Response) => {
                // login successful if there's a jwt token in the response
                    return response.json();
            })
    }

    getTable(){
        return this.http.get('http://api.football-data.org/v1/teams/66/fixtures?timeFrame=p45', options)
        .map((response: Response) => {
                    return response.json();
            })
    }
    
    };

    @NgModule({
        providers:[{provide: HTTP_INTERCEPTORS, useClass: FixturesService, multi: true}]
    })

    export class InterceptorModule{}
