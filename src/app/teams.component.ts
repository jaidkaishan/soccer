import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Request, XHRBackend, XHRConnection } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//import { FixturesService } from './fixtures.service';
import { HttpClient } from '@angular/common/http';
import { InterceptorModule } from './fixtures.service';

//require('highcharts');
declare var require: any


@Component({
    selector: 'teams-section',
    templateUrl: './teams.component.html',
    styleUrls: ['./teams.component.css']
})
export class TeamComponent {
    require: any;
    //public tableData:any[];
    public data: any[];
    public halfTimeGoals = [];
    public numberOfGoalsByHomeTeam = 0;
    public numberOfGoalsByAwayTeam = 0;
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";
    public finishedMatchesCategories = [];
    public finishedMatchedHomeTeamGoals = [];
    public finsihedMatchedAwayTeamGoals = [];
    public finsihedHomeTeamMatch = "";
    public finishedAwayTeamMatch = "";
    constructor(private http: Http, /*private fixturesService: FixturesService,*/ private httpclient: HttpClient) {

    }
    ngOnInit(): void {
        var Highcharts = require('highcharts');

        // Load module after Highcharts is loaded
        require('highcharts/modules/exporting')(Highcharts);


        const headers = new Headers();
        headers.append('X-Auth-Token', '3082de7283c74f83aea4c9348bf511ad');
        let options = new RequestOptions({ headers: headers });
        this.http.get("http://api.football-data.org/v1/competitions/444/fixtures", options)
            .map((res: Response) => {
                this.data = res.json().fixtures;
                console.log(this.data);
                for (var i = 0; i < this.data.length; ++i) {
                    this.halfTimeGoals.push(this.data[i].result);
                    if (this.data[i].result.goalsHomeTeam !== null) {
                        //console.log(this.data[i].result.halfTime.goalsHomeTeam);
                        this.numberOfGoalsByHomeTeam = this.numberOfGoalsByHomeTeam + this.data[i].result.goalsHomeTeam;
                    }
                    if (this.data[i].result.goalsAwayTeam !== null) {
                        this.numberOfGoalsByAwayTeam = this.numberOfGoalsByAwayTeam + this.data[i].result.goalsAwayTeam;
                    }
                }
                
                for (var i = 0; i < this.data.length; ++i) {
                    if (this.data[i].homeTeamName == "CR Flamengo" && this.data[i].awayTeamName == "Atlético Mineiro") {
                        this.finishedMatchesCategories.push("Home Team VS Away Team");
                        this.finishedAwayTeamMatch = this.data[i].awayTeamName;
                        this.finsihedHomeTeamMatch = this.data[i].homeTeamName;
                        this.finishedMatchedHomeTeamGoals.push(this.data[i].result.goalsHomeTeam);
                        this.finsihedMatchedAwayTeamGoals.push(this.data[i].result.goalsAwayTeam)
                    }
                }

                Highcharts.chart('chart1', {

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Number of goals scored so far by Home Team and Away Team'
                    },
                    xAxis: {
                        categories: ['Home Team', 'Away Team'],
                        title: {
                            text: 'Teams'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Goals'
                        }
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            }
                        }
                    },

                    series: [{
                        name: 'Total Goals',
                        data: [this.numberOfGoalsByHomeTeam, this.numberOfGoalsByAwayTeam]
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });

                Highcharts.chart('chart3', {

                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Finished Matches so far'
                    },
                    xAxis: {
                        categories: this.finishedMatchesCategories,
                        title: {
                            text: 'Teams'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Goals'
                        }
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            stacking: "normal"
                        }
                    },

                    series: [{
                        name: this.finsihedHomeTeamMatch,
                        data: this.finishedMatchedHomeTeamGoals
                    },
                    {
                        name: this.finishedAwayTeamMatch,
                        data: this.finsihedMatchedAwayTeamGoals
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });
            })
            .subscribe((data: any) => {
            });
    }
}