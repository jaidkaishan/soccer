import { Component, OnInit, Input } from '@angular/core';
import {FixturesService} from '../fixtures.service';
import {Fixture} from '../Fixture';

@Component({
  selector: 'app-the-last-five-games',
  templateUrl: './the-last-five-games.component.html',
  styleUrls: ['./the-last-five-games.component.css'],
  providers:[FixturesService]
})
export class TheLastFiveGamesComponent implements OnInit {

  fixtures : Fixture[];
  data: any[];

  constructor(private fixturesService: FixturesService) { }

  ngOnInit() {
  	this.TheLastFiveTables();
  }

TheLastFiveTables()
{
	console.log("Last Five Service");
	this.fixturesService.getTable().subscribe (data=> {
	this.data = data.fixtures;
  console.log(this.fixtures);
	});	

}
}

