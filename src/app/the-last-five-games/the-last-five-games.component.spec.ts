import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheLastFiveGamesComponent } from './the-last-five-games.component';

describe('TheLastFiveGamesComponent', () => {
  let component: TheLastFiveGamesComponent;
  let fixture: ComponentFixture<TheLastFiveGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheLastFiveGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheLastFiveGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
