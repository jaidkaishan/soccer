import {Result} from './result';
export class Fixture {

constructor(public date: string, public homeTeamName: string, public awayTeamName: string, public matchday: string, public result: Result) {}
}
