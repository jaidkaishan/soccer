import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CarouselModule } from 'angular4-carousel';
import { HttpModule, RequestOptions } from '@angular/http';
import { ChartModule } from 'angular2-highcharts';
import { Http } from '@angular/http';
import {DataTableModule} from "angular2-datatable";
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { TeamComponent } from './teams.component';
import { FixturesComponent } from './fixtures.component';
import { SeasonsComponent } from './seasons.component';

import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { FixturesService } from './fixtures.service';
//import { InterceptorModule } from './fixtures.service';
//declare var require: any

import { TheLastFiveGamesComponent } from './the-last-five-games/the-last-five-games.component';
import { AllGamesAndResultsComponent } from './all-games-and-results/all-games-and-results.component';
import { LastgameComponent } from './lastgame/lastgame.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TeamComponent,
    FixturesComponent,
    SeasonsComponent,
    TheLastFiveGamesComponent,
    AllGamesAndResultsComponent,
    LastgameComponent
  ],
  imports: [
    BrowserModule,
    CarouselModule,
    HttpModule,
    DataTableModule,
    ChartModule,
    HttpClientModule,
    //InterceptorModule,
    ScrollToModule.forRoot(),
    HttpClientJsonpModule
  ],
  providers: [FixturesService],
  bootstrap: [AppComponent, HeaderComponent, TeamComponent]
})
export class AppModule { }
