import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Request, XHRBackend, XHRConnection } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { FixturesService } from '../fixtures.service';
import { HttpClient } from '@angular/common/http';
//import { InterceptorModule } from './fixtures.service';

declare var require: any
@Component({
  selector: 'app-all-games-and-results',
  templateUrl: './all-games-and-results.component.html',
  styleUrls: ['./all-games-and-results.component.css'],
providers:[FixturesService]
})
export class AllGamesAndResultsComponent{
	data: any[];
  constructor(private fixturesService: FixturesService) { 
   }

  ngOnInit(): void {
  //this.TheLastgame();
  this.fixturesService.getResultsTable().subscribe( data => 
    {this.data = data.fixtures;
      console.log(this.data);
    }
  );
  }
}
