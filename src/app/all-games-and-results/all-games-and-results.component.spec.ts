import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGamesAndResultsComponent } from './all-games-and-results.component';

describe('AllGamesAndResultsComponent', () => {
  let component: AllGamesAndResultsComponent;
  let fixture: ComponentFixture<AllGamesAndResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGamesAndResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGamesAndResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
