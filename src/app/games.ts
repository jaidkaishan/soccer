import {Result} from './result';
export class Games {
constructor(public date: string, public status: string, public matchday: string, public homeTeamName: string, public awayTeamName: string, public result: Result) {}
}
